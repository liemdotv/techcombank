<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=no">
    <meta name="lang" content="vi">
    <title>Gặp 1 lần, lấy xe ngay | Techcombank</title>
    <meta name="description"
          content="Tại thời điểm chọn xe xong, bạn chỉ gặp Techcombank 1 lần để ký hồ sơ và có thể lái chiếc xe yêu thích về nhà.">
    <meta name="keywords" content="vay mua xe, vay mua o to, vay mua ô tô">
    <meta property="og:site_name" content="Techcombank">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Gặp 1 lần, lấy xe ngay | Techcombank">
    <meta property="og:description"
          content="Tại thời điểm chọn xe xong, bạn chỉ gặp Techcombank 1 lần để ký hồ sơ và có thể lái chiếc xe yêu thích về nhà.">
    <meta property="og:image" content="http://www.techcombank.com.vn:80/File/GetImage?id=0">
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WJQ9Q9');</script>
    <!-- End Google Tag Manager -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=232753167102826";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<header>
    <div class="dev-b-header">
        <div class="dev-b-container">
            <div class="dev-left-header">
                <div class="dev-logo">
                    <a href="/"><img src="images/logo.jpg" alt="logo techcombank"></a>
                </div>
            </div>
            <div class="dev-right-header">
                <div class="dev-right-wrapper">
                    <a href="tel:+841800588822">
                        <div class="dev-b-contact dev-phone">
                            <div class="dev-contact-title">
                                Cá nhân
                            </div>
                            <div class="dev-contact-number">
                                1800.588.822
                            </div>
                        </div>
                    </a>

                    <div class="dev-b-contact dev-uk-flag">
                        <img src="images/uk-flag.jpg" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="menu">
        <div class="dev-b-container">
            <div class="nav-menu">
                <ul>
                    <li class="uudiem">
                        <a href="#">
                            Ưu điểm phê duyệt trước
                        </a>
                    </li>
                    <li class="thongtin">
                        <a href=" #">
                            Thông tin vincity
                        </a>
                    </li>
                    <li class="dangky">
                        <a href="#">
                            ĐĂNG KÝ PHÊ DUYỆT TRƯỚC
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>

<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        ?>
<div class="dev-popup-wrapper">
    <div class="dev-popup">
        <div class="dev-popup-content">
            <div class="dev-body">
                <p>Cảm ơn bạn đã gửi thông tin, chúng tôi sẽ liên hệ
                    để tư vấn chi tiết theo nhu cầu của bạn
                </p>
                <div class="dev-dots">
                    <span class="dev-dot"></span>
                    <span class="dev-dot"></span>
                    <span class="dev-dot"></span>
                </div>

            </div>
        </div>
    </div>
</div>
<?php
    }

?>

<main>
    <section>
        <div class="dev-b-top-info">
            <div class="dev-b-container">
                <div class="dev-col-1-2">
                    <div class="img-top">
                        <img src="images/img-couple.jpg" alt="">
                    </div>
                </div>
                <div class="dev-col-1-2">
                    <div class="dev-content">
                        <div class="dev-head">
                            <h2><strong> Duyệt trước khoản vay đến ngay Vincity chọn nhà</strong></h2>
                        </div>
                        <div class="dev-foot">
                            <div class="dev-b-col-1-3">
                                <div class="dev-icon">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <h5><strong>Biết khoản vay trong 24h</strong></h5>
                            </div>
                            <div class="dev-b-col-1-3">
                                <div class="dev-icon">
                                    <img src="images/img2.png" alt="">
                                </div>
                                <h5><strong>Chủ động tài chính</strong></h5>
                            </div>
                            <div class="dev-b-col-1-3">
                                <div class="dev-icon">
                                    <img src="images/img3.png" alt="">
                                </div>
                                <h5><strong>Chọn nhà mơ ước</strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Phe duyet truoc-->
    <section>
        <div class="dev-b-pheduyet-wrapper">
            <div class="dev-b-container">
                <div class="top-content">
                    <h2 class="titleh2">PHÊ DUYỆT TRƯỚC GIẢI PHÁP TÀI CHÍNH ĐỘT PHÁ</h2>
                    <p> Techcombank phối hợp với Vingroup lần đầu tiên cung cấp giải pháp tài chính đột phá cho khách
                        hàng mong muốn sở hữu căn hộ tại Vincity. Bạn được cung cấp trước hạn mức tín dụng trong vòng
                        24h từ khi cung cấp đủ thông tin cho Techcombank, và hoàn toàn chủ động tài chính chọn mua căn
                        hộ ưng ý.
                    </p>
                </div>
                <div class="bot-content">
                    <div class="deb-b-col dev-b-video">
                        <div class="video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/v8lenLPNITg"
                                    frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="deb-b-col dev-b-list">
                        <ul>
                            <li>
                                <h3>Duyệt hạn mức nhanh</h3>
                                <p>Biết trước hạn mức chỉ trong 24h</p>
                            </li>
                            <li>
                                <h3>Giá trị vay cao</h3>
                                <p>Lên đến 80% giá trị căn hộ</p>
                            </li>
                            <li>
                                <h3>THỜI GIAN VAY DÀI</h3>
                                <p>Lên đến 35 năm</p>
                            </li>
                            <li>
                                <h3>Trả nợ vay linh hoạt</h3>
                                <p>Chỉ từ 5 triệu/tháng</p>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--dang ky-->
    <section>
        <div class="dev-b-dangky-wrapper">
            <div class="dev-b-container">
                <form action="#" method="post" id="form-dangky">
                    <div class="dev-row-dangky form-info">
                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Dự án dự định mua</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="duandudinhmua" name="duandudinhmua">
                            </div>
                        </div>

                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Giá trị căn hộ dự định mua</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="giatricanhodudinhmua" name="giatricanhodudinhmua">
                            </div>
                        </div>

                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Nhu cầu dự định vay</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="nhucaududinhvay" name="nhucaududinhvay">
                            </div>
                        </div>

                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Tổng thu nhập hiện tại</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="tongthunhaphientai" name="tongthunhaphientai">
                            </div>
                        </div>

                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Tổng chi phí hàng tháng</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="tongchiphihangthang" name="tongchiphihangthang">
                            </div>
                        </div>
                        <div class="dev-b-row">
                            <div class="dev-col">
                                <p>Thời gian vay dự kiến</p>
                            </div>
                            <div class="dev-col">
                                <input type="text" id="thoigianvaydukien" name="thoigianvaydukien">
                            </div>
                        </div>


                    </div>
                    <div class="dev-body">
                        <div class="title-dangky">
                            <h2 class="titleh2">Đăng ký</h2>
                            <p>Hãy để lại thông tin của bạn để Techcombank có thể tư vấn và giúp bạn sở hữu ngay căn hộ mơ ước</p>
                            <p class="red-color">Các thông tin có dấu (*) là bắt buộc</p>
                        </div>
                        <div class="dev-row-dangky">
                            <div class="dev-col">
                                <p>Họ tên <span class="red-color">*</span></p>
                            </div>
                            <div class="dev-col">
                                <input class="required" type="text" id="hoten" name="hoten">
                                <div class="error hoten_error">
                                    <p>Vui lòng nhập họ tên</p>
                                </div>
                            </div>
                        </div>
                        <div class="dev-row-dangky">
                            <div class="dev-col">
                                <p>Điện thoại <span class="red-color">*</span></p>
                            </div>
                            <div class="dev-col">
                                <input class="required" type="text" id="dienthoai" name="dienthoai">
                                <div class="error dienthoai_error">
                                    <p>Vui lòng nhập điện thoại</p>
                                </div>
                            </div>
                        </div>
                        <div class="dev-row-dangky">
                            <div class="dev-col">
                                <p>Email <span class="red-color">*</span></p>
                            </div>
                            <div class="dev-col">
                                <input class="required" type="email" id="email" name="email">
                                <div class="error email_error">
                                    <p>Vui lòng nhập email</p>
                                </div>
                            </div>
                        </div>
                        <div class="dev-row-dangky">
                            <div class="dev-col">
                                <p>Tỉnh/thành đang sinh sống và làm việc <span class="red-color">*</span></p>
                            </div>
                            <div class="dev-col">
                                <select id="tinhthanh" name="tinhthanh" class="required">
                                    <option value="">Chọn tỉnh, thành phố</option>
                                    <option value="1">Hà Nội</option>
                                    <option value="2">Hồ Chí Minh</option>
                                    <option value="4">Đà Nẵng</option>
                                    <option value="54">Cần Thơ</option>
                                    <option value="3">Hải Phòng</option>
                                    <option value="50"> An Giang</option>
                                    <option value="51">Bà Rịa - Vũng Tàu</option>
                                    <option value="59">Bạc Liêu</option>
                                    <option value="18">Bắc Giang</option>
                                    <option value="11">Bắc Kạn</option>
                                    <option value="19">Bắc Ninh</option>
                                    <option value="55">Bến Tre</option>
                                    <option value="43">Bình Dương</option>
                                    <option value="36">Bình Định</option>
                                    <option value="46">Bình Thuận</option>
                                    <option value="42">Bình Phước</option>
                                    <option value="6">Cao Bằng</option>
                                    <option value="60">Cà Mau</option>
                                    <option value="39">Đắk Lắk</option>
                                    <option value="62">Đắk Nông</option>
                                    <option value="61">Điện Biên</option>
                                    <option value="47">Đồng Nai</option>
                                    <option value="49">Đồng Tháp</option>
                                    <option value="37">Gia Lai</option>
                                    <option value="5">Hà Giang</option>
                                    <option value="23">Hà Nam</option>
                                    <option value="29">Hà Tĩnh</option>
                                    <option value="20">Hải Dương</option>
                                    <option value="63">Hậu Giang</option>
                                    <option value="22">Hoà Bình</option>
                                    <option value="21">Hưng Yên</option>
                                    <option value="35">Kon Tum</option>
                                    <option value="40">Khánh Hòa</option>
                                    <option value="53">Kiên Giang</option>
                                    <option value="7">Lai Châu</option>
                                    <option value="10">Lạng Sơn</option>
                                    <option value="8">Lào Cai</option>
                                    <option value="41">Lâm Đồng</option>
                                    <option value="48">Long An</option>
                                    <option value="24">Nam Định</option>
                                    <option value="28">Nghệ An</option>
                                    <option value="26">Ninh Bình</option>
                                    <option value="44">Ninh Thuận</option>
                                    <option value="15">Phú Thọ</option>
                                    <option value="38">Phú Yên</option>
                                    <option value="58">Sóc Trăng</option>
                                    <option value="14">Sơn La</option>
                                    <option value="17">Quảng Ninh</option>
                                    <option value="30">Quảng Bình</option>
                                    <option value="31">Quảng Trị</option>
                                    <option value="33">Quảng Nam</option>
                                    <option value="34">Quảng Ngãi</option>
                                    <option value="45">Tây Ninh</option>
                                    <option value="25">Thái Bình</option>
                                    <option value="12">Thái Nguyên</option>
                                    <option value="27">Thanh Hóa</option>
                                    <option value="32">Thừa Thiên Huế</option>
                                    <option value="52">Tiền Giang</option>
                                    <option value="57">Trà Vinh</option>
                                    <option value="9">Tuyên Quang</option>
                                    <option value="16">Vĩnh Phúc</option>
                                    <option value="56">Vĩnh Long</option>
                                    <option value="13">Yên Bái</option>
                                </select>
                                <div class="error tinhthanh_error">
                                    <p>Vui lòng chọn tỉnh/thành đang sinh sống và làm việc</p>
                                </div>
                            </div>
                        </div>

                        <div class="dev-radio-btn">
                            <span>Hộ khẩu hoặc KT3 tại tỉnh thành đang sinh sống <span class="red-color">*</span></span>
                            <span class="mobile-block">
                                <label>
                                    <input name="hokhaukt3" type="radio" value="Có" checked>Có
                                </label>
                                <label>
                                    <input name="hokhaukt3" type="radio" value="không">không
                                </label>
                            </span>
                        </div>
                        <div class="dev-radio-btn">
                            <span>Tôi có nhu cầu <span class="red-color">*</span></span>
                            <span class="mobile-block">
                                <label>
                                    <input id="timhieupheduyettruoc" name="depend_block_nhucau" type="radio" value="TÌM HIỂU PHÊ DUYỆT TRƯỚC" checked>TÌM HIỂU PHÊ DUYỆT TRƯỚC
                                </label>
                                <label>
                                    <input id="dangkymuaduanvincity" name="depend_block_nhucau" type="radio" value="ĐĂNG KÝ VAY MUA DỰ ÁN VINCITY">ĐĂNG KÝ VAY MUA DỰ ÁN VINCITY
                                </label>
                            </span>
                        </div>
                        <div class="dev-khoanvay">
                            <h4>Khoản vay</h4>
                            <div class="dev-row-dangky">
                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Giá trị căn hộ (dự kiến)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="giatricanho" id="giatricanho">
                                    </div>
                                </div>
                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Số tiền dự kiến vay (VNĐ)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="sotiendukien" id="sotiendukien">
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thời gian dự kiến vay (tháng)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="thoigiandukienvay" id="thoigiandukienvay">
                                    </div>
                                </div>
                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Nguồn thu nhập</p>
                                    </div>
                                    <div class="dev-col dev-thunhap">
                                        <label>
                                            <input id="thunhaptuluong_checkbox" type="checkbox" value="Tôi có thu nhập từ lương">Tôi có thu nhập
                                            từ lương
                                        </label>
                                        <label>
                                            <input id="thunhaptuchothuetaisan_checkbox" type="checkbox"
                                                   value="Tôi có thu nhập từ cho thuê tài sản (nhà, ô tô) ">Tôi có
                                            thu nhập từ cho thuê tài sản (nhà, ô tô)
                                        </label>
                                        <label>
                                            <input id="thunhaptucongtysohuu_checkbox" type="checkbox" value="Tôi có thu nhập từ công ty sở hữu, góp vốn  ">Tôi có thu
                                            nhập từ công ty sở hữu, góp vốn
                                        </label>
                                        <label>
                                            <input id="thunhaptuhokinhdoanh_checkbox" type="checkbox" value="Tôi có thu nhập từ hộ kinh doanh đang sở hữu  ">Tôi có thu
                                            nhập từ hộ kinh doanh đang sở hữu
                                        </label>
                                    </div>
                                </div>
                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thời gian làm việc ở đơn vị hiện tại</p>
                                    </div>
                                    <div class="dev-col">
                                        <select id="thoigianlamviec" name="thoigianlamviec">
                                            <option value="">Vui lòng chọn</option>
                                            <option value="< 12 tháng">&lt; 12 tháng</option>
                                            <option value="Từ 12 đến 36 tháng">Từ 12 đến 36 tháng</option>
                                            <option value="> 36 tháng">&gt; 36 tháng</option>
                                        </select>
                                        <div class="error thoigianlamviec_error">
                                            <p>Vui lòng chọn giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thu nhập hàng tháng của bản thân và vợ/chồng (nếu có)(VNĐ)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="thunhapcuavochong" id="thunhapcuavochong">
                                        <div class="error thunhapcuavochong_error">
                                            <p>Vui lòng nhập giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Chủ sở hữu tài sản</p>
                                    </div>
                                    <div class="dev-col">
                                        <select id="chusohuutaisan" name="chusohuutaisan">
                                            <option value="">Vui lòng chọn</option>
                                            <option value="Chồng">Chồng</option>
                                            <option value="Vợ">Vợ</option>
                                        </select>
                                        <div class="error chusohuutaisan_error">
                                            <p>Vui lòng chọn giá trị</p>
                                        </div>

                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thu nhập hàng tháng từ cho thuê tài sản (VNĐ)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="thunhaptuchothuetaisan"
                                               id="thunhaptuchothuetaisan">
                                        <div class="error thunhaptuchothuetaisan_error">
                                            <p>Vui lòng nhập giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Công ty đã đăng ký kinh doanh bao lâu</p>
                                    </div>
                                    <div class="dev-col">
                                        <select id="congtykinhdoanhbaolau" name="congtykinhdoanhbaolau">
                                            <option value="">Vui lòng chọn</option>
                                            <option value="< 12 tháng">&lt; 12 tháng</option>
                                            <option value="> 12 tháng">&gt; 12 tháng</option>
                                        </select>
                                        <div class="error congtykinhdoanhbaolau_error">
                                            <p>Vui lòng chọn giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Trước khi thành lập, công ty bắt đầu từ hình thức nào</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="truockhithanhlapctybdatdaubanghinhthucnao"
                                               id="truockhithanhlapctybdatdaubanghinhthucnao">
                                        <div class="error truockhithanhlapctybdatdaubanghinhthucnao_error">
                                            <p>Vui lòng nhập giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thu nhập hàng tháng từ công ty</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="thunhaphangthangthucongty"
                                               id="thunhaphangthangthucongty">
                                        <div class="error thunhaphangthangthucongty_error">
                                            <p>Vui lòng nhập giá trị</p>
                                        </div>

                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thời gian hoạt động liên tục</p>
                                    </div>
                                    <div class="dev-col">
                                        <select id="thoigianhoatdonglientuc" name="thoigianhoatdonglientuc">
                                            <option value="">Vui lòng chọn</option>
                                            <option value="< 12 tháng">&lt; 12 tháng</option>
                                            <option value="12 đến 36 tháng">12 đến 36 tháng</option>
                                            <option value="< 36 tháng">&lt; 36 tháng</option>
                                        </select>
                                        <div class="error thoigianhoatdonglientuc_error">
                                            <p>Vui lòng chọn giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Quý khách có đăng ký hộ kinh doanh không?</p>
                                    </div>
                                    <div class="dev-col">
                                        <select id="dangkyhokinhdoanh" name="dangkyhokinhdoanh">
                                            <option value="">Vui lòng chọn</option>
                                            <option value="Có">Có</option>
                                            <option value="Không">Không</option>
                                        </select>
                                        <div class="error dangkyhokinhdoanh_error">
                                            <p>Vui lòng chọn giá trị</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="dev-b-row">
                                    <div class="dev-col">
                                        <p>Thu nhập hàng tháng từ hộ kinh doanh (VNĐ)</p>
                                    </div>
                                    <div class="dev-col">
                                        <input type="text" name="thunhaphangthangtuhokinhdoanh"
                                               id="thunhaphangthangtuhokinhdoanh">
                                        <div class="error thunhaphangthangtuhokinhdoanh_error">
                                            <p>Vui lòng nhập giá trị</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="submit-form">
                            <input type="submit" value="GỬI THÔNG TIN">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <!--khoan vay-->
    <section>
        <div class="dev-b-khoang-vay-wrapper">
            <div class="dev-b-container">
                <h2 class="titleh2">Ước tính khoản vay</h2>
                <div class="dd-row">
                    <div class="dev-col-1-2">
                        <div class="dev-g-field">
                            <label>Giá trị căn hộ (dự kiến): (*)</label>
                            <input class="number" type="text">
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field">
                            <label>Số tiền dự án vay (VND): (*)</label>
                            <input class="number" type="text">
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field">
                            <label>Thời gian dự án vay (tháng): (*)</label>
                            <input type="text">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--nguon thu nhap-->

    <section>
        <div class="dev-nguonthunhap-wrapper">
            <div class="dev-b-container">
                <div class="dev-b-title">
                    <h2 class="titleh2">Nguồn thu nhập (chọn tối thiểu 1 nguồn thu nhập)</h2>
                    <p>(Chọn ít nhất một nguồn thu phù hợp với tình hình thu nhập của bạn)</p>
                </div>
                <div class="checkbox-wrapper">
                    <label class="control control--checkbox">Tôi có thu nhập từ lương
                        <input type="checkbox" id="checkbox-1"/>
                        <div class="control__indicator"></div>
                    </label>
                    <div class="dev-b-row checkbox-1">
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thời gian làm việc ở đơn vị hiện tại (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thu nhập hàng tháng của bản thân và vợ/chồng (nếu có)(VNĐ) (*)</label>
                                <input class="number" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkbox-wrapper">
                    <label class="control control--checkbox">Tôi có thu nhập từ cho thuê tài sản (Nhà, ô tô)
                        <input type="checkbox" id="checkbox-2"/>
                        <div class="control__indicator"></div>
                    </label>
                    <div class="dev-b-row checkbox-2">
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Chủ sở hữu tài sản (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thu nhập hàng tháng từ cho thuê tài sản (VNĐ) (*)</label>
                                <input class="number" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkbox-wrapper">
                    <label class="control control--checkbox">Tôi có thu nhập từ công ty sở hữu, góp vốn
                        <input type="checkbox" id="checkbox-3"/>
                        <div class="control__indicator"></div>
                    </label>
                    <div class="dev-b-row checkbox-3">
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Công ty đã đăng ký kinh doanh bao lâu (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Trước khi thành lập, công ty bắt đầu từ hình thức nào (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thu nhập hàng tháng từ công ty (VNĐ) (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="checkbox-wrapper">
                    <label class="control control--checkbox">Tôi có thu nhập từ hộ kinh doanh đang sở hữu
                        <input type="checkbox" id="checkbox-4"/>
                        <div class="control__indicator"></div>
                    </label>
                    <div class="dev-b-row checkbox-4">
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Quý khách có đăng ký hộ kinh doanh không ? (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thời gian hoạt động liên tục (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dev-col-1-2">
                            <div class="dev-g-field">
                                <label>Thu nhập hàng tháng từ hộ kinh doanh (VNĐ) (*)</label>
                                <div class="select-custom">
                                    <select>
                                        <option value="0" aria-disabled="true" style="color: #cccccc">Click để chọn đáp
                                            án
                                        </option>
                                        <option value="1">Căn hộ quận 9</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <a href="#" class="dev-btn-gui">Gửi</a>
            </div>
        </div>
    </section>

    <!--duc an vincity-->
    <section>
        <div class="dev-b-formula-wrapper">
            <div class="dev-b-formula-tittle">
                <div class="dev-b-container">
                    <h2 class="titleh2">DỰ ÁN VINCITY</h2>
                    <p>Vincity được phát triển theo mô hình đồng bộ với hạ tầng xã hội, tiện ích đầy đủ, đi kèm không
                        gian xanh, bệnh viện, trường học. Quy hoạch Vincity thông minh, đảm bảo đầy đủ nhu cầu vui chơi
                        giải trí, chăm sóc sức khỏe cho cư dân.
                    </p>
                </div>
            </div>
            <div class="vitricanho-wrapper">
                <div class="dev-b-container">
                    <p>Bạn đang dự định mua căn hộ ở đâu?</p>
                    <div class="select-custom">
                        <select id="duandinhmuaodau">
                            <option value="Vincity Quận 9">Vincity Quận 9</option>
                            <option value="Vincity Gia Lâm">Vincity Gia Lâm</option>
                            <option value="Vincity Tây Mỗ">Vincity Tây Mỗ</option>
                            <option value="Vincity Hà Tĩnh">Vincity Hà Tĩnh</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="dev-b-info">
                <div class="dev-b-container">
                    <div class="dev-col-1-3">
                        <div class="deb-b-canho" id="canho1">
                            <figure>
                                <img src="images/canho1.jpg" alt="">
                            </figure>
                            <p class="titlep">CĂN HỘ 1 PHÒNG NGỦ</p>
                            <p>Diện tích: xxx đến xxx m2</p>
                            <p>Giá bán: từ xxx triệu đồng</p>
                        </div>
                    </div>
                    <div class="dev-col-1-3">
                        <div class="deb-b-canho" id="canho2">
                            <figure>
                                <img src="images/canho2.jpg" alt="">
                            </figure>
                            <p class="titlep">CĂN HỘ 2 PHÒNG NGỦ</p>
                            <p>Diện tích: xxx đến xxx m2</p>
                            <p>Giá bán: từ xxx triệu đồng</p>
                        </div>
                    </div>
                    <div class="dev-col-1-3">
                        <div class="deb-b-canho" id="canho3">
                            <figure>
                                <img src="images/canho3.jpg" alt="">
                            </figure>
                            <p class="titlep">CĂN HỘ 3 PHÒNG NGỦ</p>
                            <p>Diện tích: xxx đến xxx m2</p>
                            <p>Giá bán: từ xxx triệu đồng</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dev-b-formula">
                <div class="dev-b-container">
                    <div class="dev-text">Bạn có thể sử dụng công cụ dưới đây để ước tính khả năng tài chính của mình
                    </div>
                </div>
                <div class="dev-b-container">
                    <div class="dev-col-1-2">
                        <div class="dev-g-field" id="GiaTriCanHoMua">
                            <p class="dev-field-title">Giá trị căn hộ dự định mua</p>
                            <div class="range-slider">
                                <p>
                                    <span>Số tiền </span>
                                    <span class="red-color range-slider__value">800 </span>
                                    <span class="red-color">Triệu VNĐ </span>
                                </p>
                                <input class="range-slider__range" type="range" value="800" min="500" max="2000"
                                       step="1">
                                <div class="limit-val">
                                    <div class="min-val">
                                        500 Triệu
                                    </div>
                                    <div class="max-val">
                                        2,000 Triệu
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field" id="NhuCauVay">
                            <p class="dev-field-title">Nhu cầu vay</p>
                            <div class="range-slider">
                                <p>
                                    <span>Tỷ lệ % </span>
                                    <span class="red-color range-slider__value">50 </span>
                                    <span class="red-color">% </span>
                                </p>
                                <input class="range-slider__range" type="range" value="50" min="10" max="80" step="1">
                                <div class="limit-val">
                                    <div class="min-val">
                                        10
                                    </div>
                                    <div class="max-val">
                                        80
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field" id="TongThuNhap">
                            <p class="dev-field-title">Tổng thu nhập hàng tháng của hộ gia đình</p>
                            <div class="range-slider">
                                <p>
                                    <span>Số tiền </span>
                                    <span class="red-color range-slider__value"> 20 </span>
                                    <span class="red-color">Triệu VNĐ </span>
                                </p>
                                <input class="range-slider__range" type="range" value="20" min="1" max="100" step="1">
                                <div class="limit-val">
                                    <div class="min-val">
                                        1 Triệu
                                    </div>
                                    <div class="max-val">
                                        100 Triệu
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field" id="TongChiPhi">
                            <p class="dev-field-title">Tổng chi phí hàng tháng của hộ gia đình</p>
                            <div class="range-slider">
                                <p>
                                    <span>Số tiền </span>
                                    <span class="red-color range-slider__value">10 </span>
                                    <span class="red-color">Triệu VNĐ </span>
                                </p>
                                <input class="range-slider__range" type="range" value="10" min="1" max="100" step="1">
                                <div class="limit-val">
                                    <div class="min-val">
                                        1 Triệu
                                    </div>
                                    <div class="max-val">
                                        100 Triệu
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="dev-col-1-2">
                        <div class="dev-g-field" id="ThoiHanVay">
                            <p class="dev-field-title">Thời hạn vay</p>
                            <div class="range-slider">
                                <p>
                                    <span>Thời gian: </span>
                                    <span class="red-color range-slider__value">20 </span>
                                    <span class="red-color"> Năm </span>
                                </p>
                                <input class="range-slider__range" type="range" value="20" min="1" max="35" step="1">
                                <div class="limit-val">
                                    <div class="min-val">
                                        1
                                    </div>
                                    <div class="max-val">
                                        35
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dev-col-1-2">
                        <div class="dev-g-field dev-b-guess">
                            <p class="dev-guess-title"><strong>Ước tính số tiền trả hàng tháng</strong></p>
                            <div class="dev-guess-num">
                                <span id="TienTraHangThang">5,118,721 </span>
                                <span class="red-color">VNĐ </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn-custom btn-dk"><span>Đăng ký duyệt trước khoản vay</span></a>
        </div>
    </section>

    <!--news infomation-->
    <section>
        <div class="dev-b-news-info">
            <div class="dev-b-container">
                <h2 class="titleh2">THÔNG TIN CHI TIẾT CHƯƠNG TRÌNH</h2>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/hinh1.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Tiện ích tài chính dành riêng cho khách hàng vay mua căn hộ tại dự án Vincity
                            </a>
                        </div>

                    </div>
                </div>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/hinh2.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Techcombank Pre-Process: Quy trình rút gọn, lựa chọn dễ dàng
                            </a>
                        </div>

                    </div>
                </div>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/hinh3.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Vay vốn mua nhà: Cơ hội mới để sở hữu ngôi nhà mơ ước cho các gia đình trẻ
                            </a>
                        </div>

                    </div>
                </div>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/news4.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Căn hộ Vincity Quận 7
                            </a>
                        </div>

                    </div>
                </div>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/news5.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Căn hộ Vincity Quận 9
                            </a>
                        </div>

                    </div>
                </div>
                <div class="dev-b-col-1-3">
                    <div class="dev-b-news-info-item">
                        <div class="thumb-news-info">
                            <a href="#"><img src="images/news6.jpg" alt=""></a>
                        </div>
                        <div class="dev-news-info-title">
                            <a href="#">
                                Căn hộ Vincity Gia lâm
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="dev-b-footer">
        <div class="dev-b-container">
            <div class="dd-body">
                Bản quyền © 2017 thuộc về Ngân hàng Techcombank. Bảo lưu mọi quyền lợi
            </div>
        </div>
    </div>
</footer>
<script src="https://www.techcombank.com.vn/Content/js/jquery/jquery-1.9.1.js"></script>
<script src="js/main.js"></script>

</body>
</html>