
(function($){


    var tongThuNhapDefault = 20;
    var giaTriCanHoMuaDefault = 800;
    var tongChiPhiDefault = 10;
    var nhuCauVayDefault = 50;
    var thoiHanVayDefault = 20;
    var laiSuatVay = 10.5;

    $('#TongThuNhap .range-slider__range').val(tongThuNhapDefault);
    $('#TongThuNhap .range-slider__value').html(tongThuNhapDefault);


    $('#GiaTriCanHoMua .range-slider__range').val(giaTriCanHoMuaDefault);
    $('#GiaTriCanHoMua .range-slider__value').html(giaTriCanHoMuaDefault);

    $('#TongChiPhi .range-slider__range').val(tongChiPhiDefault);
    $('#TongChiPhi .range-slider__value').val(tongChiPhiDefault);

    $('#NhuCauVay .range-slider__range').val(nhuCauVayDefault);
    $('#NhuCauVay .range-slider__value').val(nhuCauVayDefault);

    $('#ThoiHanVay .range-slider__range').val(thoiHanVayDefault);
    $('#ThoiHanVay .range-slider__value').val(thoiHanVayDefault);


    var tongThuNhap = $('#TongThuNhap .range-slider__range').val();
    var giaTriCanHoMua = $('#GiaTriCanHoMua .range-slider__range').val();
    var tongChiPhi = $('#TongChiPhi .range-slider__range').val();
    var nhuCauVay = $('#NhuCauVay .range-slider__range').val();
    var thoiHanVay = $('#ThoiHanVay .range-slider__range').val(); //tinh theo nam
    $(document).ready(function(){

        Calculator(laiSuatVay, tongThuNhap, tongChiPhi, giaTriCanHoMua, nhuCauVay, thoiHanVay );


        $(".deb-b-canho").on("click" , function(){
            $('#NhuCauVay .range-slider__range').val(nhuCauVayDefault);
            $('#ThoiHanVay .range-slider__range').val(thoiHanVayDefault);
            $("#NhuCauVay .range-slider__value").html(nhuCauVayDefault);
            $("#ThoiHanVay .range-slider__value").html(thoiHanVayDefault);

            var id = this.id;
            $(".deb-b-canho").removeClass("active");
            $(this).addClass("active");

            switch (id){
                case "canho1":{
                    giaTriCanHoMua = 800;
                    break;
                }
                case "canho2":{
                    giaTriCanHoMua = 1000;

                    break;
                }
                case "canho3":{
                    giaTriCanHoMua = 1500;
                    break;
                }
            }

            var moneyVal =  giaTriCanHoMua.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $("#GiaTriCanHoMua .range-slider__value").html(moneyVal);

            $('#GiaTriCanHoMua .range-slider__range').val(giaTriCanHoMua);
            Calculator(laiSuatVay, tongThuNhap, tongChiPhi, giaTriCanHoMua, nhuCauVay, thoiHanVay );


        });
        rangeSlider('TongThuNhap');
        rangeSlider('TongChiPhi');
        rangeSlider('GiaTriCanHoMua');
        rangeSlider('NhuCauVay');
        rangeSlider('ThoiHanVay');

        $('.uudiem').on('click', function(){
            $('html, body').animate({scrollTop: '0px'}, 1000, 'linear');
        });
        $('.thongtin').on('click', function(){
            $('html, body').animate({scrollTop: $('.dev-b-formula-wrapper').position().top}, 500, 'linear');
        });
        $('.dangky').on('click', function(){
            $('html, body').animate({scrollTop: $('.dev-b-dangky-wrapper').position().top}, 500, 'linear');
        });


        $('.checkbox-wrapper input[type = checkbox]').on('click', function(){
            var id = this.id;
            if($('.' + id + ':visible').length == 0){
                $('.' + id).fadeIn()
            }else{
                $('.' + id).fadeOut();
            }
        });


        $(document).bind( "mouseup touchend", function(e){
            var container = $('.dev-popup-content');
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $('.dev-popup-wrapper').fadeOut();
            }
        });

        $('.btn-timhieu').on('click', function(e){
            e.preventDefault();
            $('.btn-custom').removeClass('active');
            $(this).addClass('active');
            $('.dev-b-khoang-vay-wrapper').fadeOut();
            $('.dev-nguonthunhap-wrapper').fadeOut();
        })
        $('.btn-dangky').on('click', function(e){
            e.preventDefault();
            $('.btn-custom').removeClass('active');
            $(this).addClass('active');
            $('.dev-b-khoang-vay-wrapper').fadeIn();
            $('.dev-nguonthunhap-wrapper').fadeIn();
        });

        $("input").keyup(function(e){
            if(e.keyCode>=48 && e.keyCode<=57){
                var $this = $(this);
                if ((($this.val().length+1) % 4)==0){
                    $this.val($this.val() + " ");
                }
            }
        });



        $('.dev-thunhap label').on('click', function () {
            if($('#thunhaptuluong_checkbox').is(':checked')) {
                $('#thunhapcuavochong').addClass('required');
            }else {
                $('#thunhapcuavochong').removeClass('required');
                $('#thunhapcuavochong').removeClass('errorbox');
                $('.thunhapcuavochong_error').removeClass('active');

            }

            if($('#thunhaptuchothuetaisan_checkbox').is(':checked')) {
                $('#thunhaptuchothuetaisan').addClass('required');
            }else {
                $('#thunhaptuchothuetaisan').removeClass('required');
                $('#thunhaptuchothuetaisan').removeClass('errorbox');
                $('.thunhaptuchothuetaisan_error').removeClass('active');

            }

            if($('#thunhaptucongtysohuu_checkbox').is(':checked')) {
                $('#thunhaphangthangthucongty').addClass('required')
            }else {
                $('#thunhaphangthangthucongty').removeClass('required');
                $('#thunhaphangthangthucongty').removeClass('errorbox');
                $('.thunhaphangthangthucongty_error').removeClass('active');

            }

            if($('#thunhaptuhokinhdoanh_checkbox').is(':checked')) {
                $('#thunhaphangthangtuhokinhdoanh').addClass('required')
            }else {
                $('#thunhaphangthangtuhokinhdoanh').removeClass('required');
                $('#thunhaphangthangtuhokinhdoanh').removeClass('errorbox');
                $('.thunhaphangthangtuhokinhdoanh_error').removeClass('active');

            }

        });



        $('.dev-radio-btn input[type=radio]').click(function() {
            if($('#dangkymuaduanvincity').is(':checked')) {
                $('.dev-khoanvay').addClass('active');
            }else{
                $('.dev-khoanvay').removeClass('active');
            }
        });
        $('#duandinhmuaodau').on('change', function () {
            $('#duandudinhmua').val($('#duandinhmuaodau').val());
        });

        $('#form-dangky').submit( function (e) {
            $( ".required" ).each(function( index ) {
                var id = this.id;
                if($(this).val() == ""){
                    $('.' + id + '_error').addClass('active');
                    $(this).addClass('errorbox');
                    e.preventDefault();
                }else{
                    $('.' + id + '_error').removeClass('active');
                    $(this).removeClass('errorbox');
                }
            });
            // e.preventDefault();
        })



    }); //end document ready

    rangeSlider = function($id){
        var slider = $('#'+ $id +' .range-slider'),
            range = $('#'+ $id +' .range-slider__range'),
            value = $('#'+ $id +' .range-slider__value');
        slider.each(function(){
            value.each(function(){
                var value = $(this).prev().attr('value');
                $(this).html(value);
            });
            range.on('input', function(){
                var moneyVal =  this.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                value.html(moneyVal);
                tongThuNhap = $('#TongThuNhap .range-slider__range').val();
                giaTriCanHoMua = $('#GiaTriCanHoMua .range-slider__range').val();
                tongChiPhi = $('#TongChiPhi .range-slider__range').val();
                nhuCauVay = $('#NhuCauVay .range-slider__range').val();
                thoiHanVay = $('#ThoiHanVay .range-slider__range').val(); //tinh theo nam

                Calculator(laiSuatVay, tongThuNhap, tongChiPhi, giaTriCanHoMua, nhuCauVay, thoiHanVay );
            });

            //for ie
            range.on('change', function(){
                var moneyVal =  this.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                value.html(moneyVal);
                tongThuNhap = $('#TongThuNhap .range-slider__range').val();
                giaTriCanHoMua = $('#GiaTriCanHoMua .range-slider__range').val();
                tongChiPhi = $('#TongChiPhi .range-slider__range').val();
                nhuCauVay = $('#NhuCauVay .range-slider__range').val();
                thoiHanVay = $('#ThoiHanVay .range-slider__range').val(); //tinh theo nam

                Calculator(laiSuatVay, tongThuNhap, tongChiPhi, giaTriCanHoMua, nhuCauVay, thoiHanVay );
            });
        });
    };

    Calculator = function (laiSuatVay, tongThuNhap, tongChiPhi, giaTriCanHoMua, nhuCauVay, thoiHanVay ) {
        // Calculator
        var soTienConLai = tongThuNhap - tongChiPhi;
        var soTienCanVay = giaTriCanHoMua * nhuCauVay / 100;
        var soKyTraNo = thoiHanVay * 12; //theo thang
        var tienGocPhaiTra = soTienCanVay / soKyTraNo; //theo thang
        var tienLaiPhaiTra = (soTienCanVay * (laiSuatVay / 100) * 30) / 365; // theo thang
        var tienTraHangThang = (tienGocPhaiTra + tienLaiPhaiTra) * 1000000;
        var tienTraHangThangFormat =  tienTraHangThang.toFixed(0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('#TienTraHangThang').html(tienTraHangThangFormat);


        $('#duandudinhmua').val($('#duandinhmuaodau').val());
        $('#giatricanhodudinhmua').val(giaTriCanHoMua);
        $('#nhucaududinhvay').val(nhuCauVay);
        $('#tongthunhaphientai').val(tongThuNhap);
        $('#tongchiphihangthang').val(tongChiPhi);
        $('#thoigianvaydukien').val(thoiHanVay);

    };



    /*
     CSS Browser Selector v0.4.0 (Nov 02, 2010)
     Rafael Lima (http://rafael.adm.br)
     http://rafael.adm.br/css_browser_selector
     License: http://creativecommons.org/licenses/by/2.5/
     Contributors: http://rafael.adm.br/css_browser_selector#contributors
     */

    function css_browser_selector(u) {
        var ua = u.toLowerCase(),
            is = function (t) {
                return ua.indexOf(t) > -1
            }, g = 'gecko webfont',
            w = 'webkit',
            s = 'safari',
            o = 'opera',
            m = 'mobile',
            tl = 'tablet',
            h = document.documentElement,
            b = [(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) ? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' : is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 : (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) : is('konqueror') ? 'konqueror' : is('blackberry') ? m + ' blackberry' : is('android') ? m + ' android webfont' : is('chrome') ? w + ' chrome' : is('iron') ? w + ' iron' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : (/trident/.test(ua)) ? 'ie ie11' : is('mozilla/') ? g : '', is('j2me') ? m + ' j2me' : is('iphone') ? m + ' iphone' : is('ipod') ? m + ' ipod' : is('ipad') ? m + ' ipad webfont' : is('mac') ? 'mac webfont' : is('darwin') ? 'mac webfont' : is('webtv') ? 'webtv webfont' : is('win') ? 'win' + (is('windows nt 5') ? 'xp webfont' : 'new') : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux webfont' : '', is('tablet') ? tl : '', 'js'];
        c = b.join(' ');
        h.className += ' ' + c;
        return c;
    };
    css_browser_selector(navigator.userAgent);



})(jQuery);
